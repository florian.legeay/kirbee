const { v4: uuidv4 } = require('uuid');

function createGame() {
    return {
        id: 'game.' + uuidv4(),
        createdAt: new Date(),
        players: [],
        isFull: false,
        isReady: false,
        startedAt: null,
        fireAt: null,
        winner: null,
    }
}

function getWinner(game, socketId, toggle) {
    console.log('game', game);

    // Pressed too soon ?
    if (toggle.toggleExclamationPoint === true && game.winner === null && game.startedAt !== null) {
        game.winner = game.players.filter((p) => p.heroId === socketId)[0];
    }
    else {
        game.winner = game.players.filter((p) => p.heroId !== socketId)[0];
    }

    return game.winner;
}

function launchGame(game) {
    game.isReady = true;
    game.startedAt = Date.now();
    game.delay = (Math.random() * (6 - 2) + 2) * 1000;
    game.fireAt = Date.now() + game.delay;
    return game;
}

module.exports = { createGame, getWinner, launchGame }
