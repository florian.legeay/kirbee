const {v4: uuidv4} = require('uuid');

function createPlayer(name, heroId) {
    return {
        id: uuidv4(),
        name,
        heroId,
        ready: false,
    }
};

module.exports = { createPlayer }
