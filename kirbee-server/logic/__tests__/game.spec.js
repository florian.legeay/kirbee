const Game = require('../game');
const Player = require('../player');


describe("getWinner", () => {
    const game = Game.createGame();
    game.players.push(Player.createPlayer('Kirbee', 1));
    game.players.push(Player.createPlayer('Ninja', 2));
    game.startedAt = Date.now();
    const toggle = {
        toggleExclamationPoint: true,
    };

    it("[unit] It should return Kirbee with ExclamationPoint toggled & Kirbee pressed a button", () => {
        game.winner = null;
        const winner = Game.getWinner(game, 1, toggle);
        expect(winner.name).toBe('Kirbee');
    });
    it("[unit] It should return Ninja with ExclamationPoint not toggled & Kirbee pressed a button", () => {
        game.winner = null;
        toggle.toggleExclamationPoint = false;
        const winner = Game.getWinner(game, 1, toggle);
        expect(winner.name).toBe('Ninja');
    });
});