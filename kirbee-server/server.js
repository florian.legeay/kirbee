const Player = require('./logic/player');
const Game = require('./logic/game');

const io = require('socket.io')(3006, {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
});

io.on('connection', (socket) => {
  // When a player join the game
  socket.on('player.join', () => {
    // If he already joined => emit an error
    if (socket.adapter.room?.game?.players?.filter((p) => p.heroId === socket.id).length === 1) {
      socket.emit('error', {
        has_error: true,
        error: 'ALREADY_JOINDED',
      });
      return;
    }

    // If we already have 2 players => emit an error
    if (socket.adapter.room?.game?.isFull) {
      socket.emit('error', {
        has_error: true,
        error: 'TOO_MANY_PLAYERS',
      });
      return;
    }

    // If no game has been created => Create the game
    if (!socket.adapter.room) {
      const newGame = Game.createGame();
      socket.adapter.room = {
        game: newGame,
      }
    };

    // Create the player, Kirbee OR Ninja
    let myPlayer;
    if (socket.adapter.room.game.players.filter((p) => p.name === 'Kirbee').length === 1)
      myPlayer = Player.createPlayer('Ninja', socket.id);
    else myPlayer = Player.createPlayer('Kirbee', socket.id)

    socket.adapter.room.game.players.push(myPlayer);

    // If we have two players in the room, set game as full
    if (socket.adapter.room.game.players.length === 2) socket.adapter.room.game.isFull = true;

    // The player join the room
    socket.join(socket.adapter.room.game.id);
    socket.emit('player.joined', {
      name: myPlayer.name,
      game: socket.adapter.room.game.id
    });
  });

  // When a player set as ready
  socket.on('player.ready', () => {
    // We set the player as ready
    socket.adapter.room.game.players.filter((p) => p.heroId === socket.id)[0].ready = true;
    socket.adapter.room.game.winner = null;

    // If we have 2 players ready -> Let's GO !
    if (socket.adapter.room.game.players.filter((p) => p.ready === true).length === 2) {
      socket.adapter.room.game = Game.launchGame(socket.adapter.room.game)
      io.sockets.in(socket.adapter.room.game.id).emit('game.ready', (socket.adapter.room.game));
    }
  });

  // When a key is pressed
  socket.on('player.keypressed', (toggle) => {
    socket.adapter.room.game.winner = Game.getWinner(socket.adapter.room.game, socket.id, toggle);

    socket.adapter.room.game.players.forEach(p => {
      p.ready = false;
    });

    // Send info of the Winner
    io.sockets.in(socket.adapter.room.game.id).emit('game.end', socket.adapter.room.game.winner);
  });

  // When a player disconnect
  socket.on('disconnect', () => {
    if (socket.adapter.room?.game?.players?.length > 0) {
      socket.adapter.room.game.players = socket.adapter.room.game?.players?.filter((p) => p.heroId !== socket.id);
      socket.adapter.room.game.isFull = false;
    }
    if (socket.adapter.room?.game)
      io.sockets.in(socket.adapter.room?.game?.id).emit('opponent.disconnected');
  });
});
